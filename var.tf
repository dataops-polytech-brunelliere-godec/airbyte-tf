variable "snowflake_account" {
  description = "Snowflake account"
  type        = string
  sensitive   = true
}

variable "airbyte_user" {
  description = "Airbyte user"
  type        = string
  sensitive   = true
}
variable "airbyte_password" {
  description = "Airbyte password"
  type        = string
  sensitive   = true
}

variable "ingestion_password" {
  description = "Ingestion password"
  type        = string
  sensitive   = true
}

variable "workspace_id" {
  description = "Airbyte worksapce id"
  type        = string
  sensitive   = true
}
