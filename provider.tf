terraform {
  required_providers {
    airbyte = {
      source  = "airbytehq/airbyte"
      version = "0.4.1"
    }
  }
}

provider "airbyte" {
  username   = var.airbyte_user
  password   = var.airbyte_password
  server_url = "http://localhost:8006/v1"
}
