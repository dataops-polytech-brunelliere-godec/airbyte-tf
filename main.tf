resource "airbyte_source_faker" "FAKER_SOURCE" {
  configuration = {
    always_updated    = false
    count             = 100
    parallelism       = 8
    records_per_slice = 100
    seed              = 7
  }
  name         = "dev-faker-instance"
  workspace_id = var.workspace_id
}

resource "airbyte_destination_snowflake" "SNOWFLAKE_DESTINATION" {
  configuration = {
    credentials = {
      username_and_password = {
        password = var.ingestion_password
      }
    }
    database  = "BRONZE"
    host      = format("%s.%s", var.snowflake_account, "snowflakecomputing.com")
    role      = "ROLE_INGESTION"
    schema    = "AIRBYTE"
    username  = "INGESTION_USER"
    warehouse = "INGESTION_WAREHOUSE"
  }
  name         = "dev-snowflake-instance"
  workspace_id = var.workspace_id
}

resource "airbyte_connection" "CONNECTION" {
  destination_id = airbyte_destination_snowflake.SNOWFLAKE_DESTINATION.destination_id
  source_id      = airbyte_source_faker.FAKER_SOURCE.source_id
}
